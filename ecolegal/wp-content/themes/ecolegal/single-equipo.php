<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="nuestros-miembros"]');?>
</div>

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-md-12  miembros-padding wow fadeInDown" data-wow-offset="10" data-wow-duration="2s">
      <center>
      <?php if(has_post_thumbnail()) { ?>
          <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-equipo-trabajo-1">
      <?php }else { ?>
          <img class="img-responsive" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/09/default-profile.png" alt="imagen-equipo-trabajo-1">
      <?php } ?>
      <br>
      <p class="title-upper text-gray miembros-title">  <?php echo the_title(); ?> </p>
      <span class="title-upper text-gray"><?php the_content() ?></span>
        <p>
          <a class="hvr-grow" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
        </p>
        <p>
          <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
        </p>

          <strong><p>Experiencia:</p></strong>
          <p class="text-justify"><?php echo get_field('experiencia'); ?></p>  </center>
        <strong><p> Áreas de Práctica: </p></strong>
          <?php if(have_rows('areas_de_practica')):?>
            <ul>
          <?php while(have_rows('areas_de_practica')) : the_row();
              $descripcion = get_sub_field('descripcion_area_practica',$id);
              ?>
                <li> <?php echo $descripcion; ?></li>
              <?php
                endwhile;
              ?>
           </ul>
          <?php
          else :
          endif;
          ?>

          <strong><p>Educación:</p></strong>
          <?php if(have_rows('educacion')):?>
          <ul>
            <?php while(have_rows('educacion')) : the_row(); $descripcion = get_sub_field('descripcion_educacion',$id); ?>
                <li> <?php echo $descripcion; ?></li>
              <?php
                endwhile;
              ?>
           </ul>
          <?php
          else :
          endif;
          ?>

        <strong><p>Idiomas:</p> </strong>

          <?php if(have_rows('idiomas')):?>
            <ul>
          <?php while(have_rows('idiomas')) : the_row();
              $descripcion = get_sub_field('descripcion_idiomas',$id);
              ?>
                <li> <?php echo $descripcion; ?></li>
              <?php
                endwhile;
              ?>
           </ul>
          <?php
          else :
          endif;
          ?>

          <strong><p>Membresías:</p></strong>
          <?php if(have_rows('membresias')):?>
            <ul>
          <?php while(have_rows('membresias')) : the_row();
              $descripcion = get_sub_field('descripcion_membresias',$id);
              ?>
                <li> <?php echo $descripcion; ?></li>
              <?php
                endwhile;
              ?>
           </ul>
          <?php
          else :
          endif;
          ?>




    </div>
  </div>
</div>

<?php get_footer("2"); ?>
