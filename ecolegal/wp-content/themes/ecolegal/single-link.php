
<?php get_header()?>


<div class="row">
  <?php echo do_shortcode('[rev_slider alias="link"]');?>
</div>

<div class="container" id="clientesid">
  <div class="row">

    <div class="col-xs-12 col-md-12">
      <?php
        $id= get_the_ID();
        $post = get_post($id);
        $content= apply_filters('the_content', $post->post_content);
        $title = $post->post_title;
        $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
      ?>
      <div class="item-content-logo animated fadeInDown">
        <center>
        
            <?php
              $image = get_field('imagen');
              $url = $image['url'];
            ?>
            <a class="hvr-grow" href="<?php echo get_field('url-cliente'); ?>"  target="_blank" title=" <?php echo the_title(); ?>">
              <img class="brand-size" src="<?php echo $url; ?>" alt="imagen-links">
            </a>

      </center>
      </div>

      </div>
    </div>
  </div>
<?php get_footer("2"); ?>
