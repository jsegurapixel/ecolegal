<?php /* Template Name: Clientes-actuales */ ?>

<?php get_header()?>


<div class="row">
  <?php echo do_shortcode('[rev_slider alias="clientes-actuales"]');?>
</div>

<div class="container" id="clientesid">
  <div class="row page-padding">
  </div>
  <div class="row">

    <div class="col-xs-12 col-md-12">
        <div class='container-slick'>
          <!-- imagenes -->
          <div class='item-clientes'>
              <?php

                $args = array(
                    'posts_per_page' => -1,
                      'post_type' => 'cliente',
                      'orderby' => 'title',
                      'order' => 'ASC',
                    );
                $the_query = new WP_Query( $args );?>
              <?php if ( $the_query->have_posts() ) : ?>
              <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                    <div class="item-content-logo animated fadeInDown">
                      <?php
                        $image = get_field('logo');
                        $url = $image['url'];
                      ?>
                      <a href=" <?php echo get_field('url'); ?>" target="_blank">
                        <img class="brand-size hvr-float" src="<?php echo $url; ?>" alt="imagen-clientes-actuales">
                      </a><br>
                    </div>
              <?php endwhile; ?>
              <?php else : ?>
                <p><?php _e( 'No hay información para mostrar.' ); ?></p>
              <?php endif; ?>
          </div>
          <!-- arrows -->
          <div>
            <center>
            <ul class="ul-line-display pagination-icon">
              <li id="prevId" class="li-line-arrows hvr-float"> < </li>
              <li id="nextId" class="li-line-arrows hvr-float"> > </li>
            </ul>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer("2"); ?>
