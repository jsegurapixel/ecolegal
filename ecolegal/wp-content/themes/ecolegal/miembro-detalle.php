<?php /* Template Name: miembro-detalle */ ?>

<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="nuestros-miembros"]');?>
</div>


<?php
  $id= $_GET['id'];
  $post = get_post($id);
  $content= apply_filters('the_content', $post->post_content);
  $title = $post->post_title;
  $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-12 page-padding">
          <div class="col-xs-12 col-md-4 no-padding wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s">
              <?php if(has_post_thumbnail()) { ?>
                <img class="img-responsive img-miembro" src="<?php echo $img_nosotros_home_url; ?>" alt="imagen-equipo-trabajo-1">
            <?php }else { ?>
                <img class="img-responsive img-miembro" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/09/default-profile.png" alt="imagen-equipo-trabajo-1">
            <?php } ?>
          </div>
          <div class="col-xs-12 col-md-8">
            <div class="row header-miembro title-upper">
              <h2 class="wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> <?php echo $title; ?></h2>
               <p class="wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> <?php echo  $content; ?></p>
            </div>
            <div class="row page-padding">
              <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-6 txt-center">
                  <p>
                    <a class="hvr-grow wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
                  </p>
                </div>
                <div class="col-xs-12 col-md-6 txt-center">
                  <p>
                    <?php
                      if (get_field('telefono') != null) {
                    ?>
                    <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
                  <?php } ?>
                  </p>
                </div>
              </div>
              <?php if(get_field('experiencia')) { ?>
                <strong><p class="title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s">Experiencia</p></strong>
                <p class="text-justify wow slideInDown" data-wow-offset="10" data-wow-duration="1.4s"><?php echo get_field('experiencia'); ?></p>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-12">
          <div class="col-xs-12 col-md-4 page-padding miembro-content">
            <?php if(have_rows('areas_de_practica')):?>
              <strong><p class="title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> Áreas de Práctica </p></strong>
              <ul>
            <?php while(have_rows('areas_de_practica')) : the_row();
                $descripcion = get_sub_field('descripcion_area_practica',$id);
                ?>
                  <li class=" wow slideInDown" data-wow-offset="10" data-wow-duration="1.4s"> <?php echo $descripcion; ?></li>
                <?php
                  endwhile;
                ?>
             </ul>
            <?php
            else :
            endif;
            ?>
          </div>
          <div class="col-xs-12 col-md-4 page-padding miembro-content">
             <?php if(have_rows('educacion')):?>
               <strong><p class="title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s">Educación</p></strong>
               <ul>
               <?php while(have_rows('educacion')) : the_row(); $descripcion = get_sub_field('descripcion_educacion',$id); ?>
                   <li class=" wow slideInDown" data-wow-offset="10" data-wow-duration="1.4s"> <?php echo $descripcion; ?></li>
                 <?php
                   endwhile;
                 ?>
              </ul>
             <?php
             else :
             endif;
             ?>
          </div>
          <div class="col-xs-12 col-md-4 page-padding miembro-content">
            <?php if(have_rows('idiomas')):?>
             <strong><p class="title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s">Idiomas</p> </strong>
             <ul>
               <?php while(have_rows('idiomas')) : the_row();
               $descripcion = get_sub_field('descripcion_idiomas',$id);
               ?>
                 <li class=" wow slideInDown" data-wow-offset="10" data-wow-duration="1.4s"> <?php echo $descripcion; ?></li>
               <?php
                 endwhile;
               ?>
            </ul>
           <?php
           else :
           endif;
           ?>
          </div>
        </div>
    </div>
</div>

<div class="container-fluid no-padding">
  <div class="row">
    <div class='container-1'>
      <div class='item-miembros' >
          <?php
          $args = array(
            'posts_per_page' => -1,
            'post_type' => 'equipo',
            'orderby' => 'rand',
            'order' => 'ASC',
          );
          $the_query = new WP_Query( $args );
          $cont = 0; ?>
          <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <div class="">
            <?php if(has_post_thumbnail()) { ?>
                <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-equipo-trabajo-1">
            <?php }else { ?>
                <img class="img-responsive" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/10/fondo-blanco.jpg" alt="imagen-equipo-trabajo-1">
            <?php } ?>
            <br>
            <?php if (get_field('experiencia') && have_rows('areas_de_practica') && have_rows('educacion') && have_rows('idiomas')): ?>
                <a class="title-upper text-gray miembros-title hvr-grow" href=" <?php echo $url_detalle . '?id=' . get_the_ID(); ?> ">  <?php echo the_title(); ?> </a>
              <?php else : ?>
                <p class="title-upper text-gray miembros-title">  <?php echo the_title(); ?> </p>
            <?php endif; ?>
              <span class="title-upper text-gray"><?php the_content() ?></span>
              <p>
                <a class="hvr-grow" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
              </p>
              <p>
                <?php
                  if (get_field('telefono') != null) {
                ?>
                <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
              <?php } ?>
              </p>
          </div>
        <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>

          <?php else : ?>
          <p><?php _e( 'No se ha encontrado información' ); ?></p>
          <?php endif; ?>

      </div>
    </div>
  </div>

</div>




<?php get_footer("2"); ?>
