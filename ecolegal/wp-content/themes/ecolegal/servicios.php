<?php /* Template Name: Servicios */ ?>

<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="servicio"]');?>
</div>

<div class="row container-background">
  <div class="container page-padding text-justify">
    <!-- Pagina -->
    <?php
      $id=172;
      $post = get_post($id);
      $content = apply_filters('the_content', $post->post_content);
      $title = $post->post_title;
    ?>
    <!-- información servicio -->
    <?php
      $id=122;
      $post = get_post($id);
      $title = $post->post_title;
      $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
    ?>
    <span class="wow fadeIn" data-wow-offset="10" data-wow-duration="1.7s">
      <p class="title-upper"> <?php echo  the_content($id); ?> </p>
    </span>
   </div>
</div>

<!-- Servicios -->
<div class="row text-gray background-gray">
  <div class="text-center title-upper">
    <strong><h2 class="wow zoomIn page-padding" data-wow-offset="10" data-wow-duration="1.7s" >servicios</h2></strong>
  </div>

  <?php
  $args = array(
  'posts_per_page' => -1,
  'post_type' => 'servicio',
  'orderby' => 'publish_date',
  'order' => 'ASC',
  );
  $the_query = new WP_Query( $args );?>

  <div class='container-1'>
    <div class='single-item' >
      <?php if ( $the_query->have_posts() ) : ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
      <?php
        $id= get_the_ID();
        $post = get_post($id);
      ?>
        <?php if(have_rows('tipo_servicio',$id)):?>
          <?php while(have_rows('tipo_servicio',$id)) : the_row();
            $valor = get_sub_field('logo',$id);
            $url_logo = $valor['url'];
          ?>
          <div class="single-item-background background-gray content-servicios">
            <center><img class="img-size-servicio wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s" src="<?php echo $url_logo; ?>" alt="img-logo-servicios-ecolegal"></center>
            <h2 class="text-gray text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo the_title(); ?></h2><br>
            <div class="text-justify content-font-servicios content-padding-servicios">
              <ul class="wow fadeInUp" data-wow-offset="10" data-wow-duration="1.7s">
                <?php while(have_rows('descripcion_servicio',$id)) : the_row();
                ?>
                <li>  <?php  the_sub_field('descripcion',$id); ?></li>
                <?php endwhile; ?>
              </ul>
            </div>
          </div>
          <?php
            endwhile;
          ?>

          <?php
          else :
          endif;
          ?>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

      <?php else : ?>
        <p><?php _e( 'No hay información para mostrar' ); ?></p>
      <?php endif; ?>
    </div>
    <!-- <ul class="link-arrows custome-arrows">
      <li class="hvr-grow"> <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-20.svg" alt=""> </li>
      <li class="hvr-grow"> <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-19.svg" alt=""></li>
    </ul> -->
  </div>
</div>

<?php echo get_footer("2"); ?>
