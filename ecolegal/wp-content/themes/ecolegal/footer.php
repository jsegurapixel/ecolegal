

<?php

$datos_contacto = 112;

 ?>

</div>
  <?php wp_footer();?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/functions.js"></script>

    <?php

    if ( function_exists( 'ot_get_option' ) ) {
    $fixed = ot_get_option( 'header_fixed' );

    if ($fixed == 1 ) {
  ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/header-fixed.js"></script>
  <?php
    }
    }
    ?>
  <footer>
<!--  -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-md-6 no-padding footer-map">
          <!-- <div><iframe width="100%" height="518px" style="position: absolute;
            height: 100%;"
            src="https://maps.google.com/maps?width=100%&height=500&hl=es&q=Oficentro%20Torres%20del%20Campo%20San%20Jose%20Torre%20I+(ecolegal)&ie=UTF8&t=&z=18&iwloc=A&output=embed"
            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
            <a href="https://www.mapsdirections.info/crear-un-mapa-de-google/">Crear Google Map</a> by <a href="https://www.mapsdirections.info/">Mapa España</a></iframe></div><br /> -->
        </div>
          <div class="col-xs-12 col-md-6 background-contact">
            <div class="row layer text-white">
              <div class="col-xs-12 col-md-2"></div>
              <div class="col-xs-12 col-md-10" style="height:468px">
                    <br>
                    <div class="col-xs-12 col-md-6 text-center title-upper margin-info wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> <h2>Contáctenos</h2></div>
                    <br>
                    <div class="col-xs-12 col-md-12 margin-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-14.svg">
                        <span class="icon-text"> <?php echo get_field('telefono_1', $datos_contacto) . ' | ' . get_field('telefono_2', $datos_contacto)  ; ?> </span>
                        </img>
                    </div>
                    <div class="col-xs-12 col-md-12 margin-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-03.svg">
                          <span class="icon-text"> <?php echo get_field('fax', $datos_contacto); ?> </span>
                        </img>
                    </div>
                    <div class="col-xs-12 col-md-12 margin-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-13.svg">
                        <span class="icon-text"> <?php echo get_field('correo_electronico', $datos_contacto); ?> </span>
                      </img>
                    </div>
                    <div class="col-xs-12 col-md-12 margin-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-04.svg">
                          <span class="icon-text"> <?php echo get_field('direccion', $datos_contacto); ?> </span>
                        </img>
                    </div>
                </div>

                <div class="text-white">
                  <div class="col-xs-12 col-md-12 text-center social-media">
                      <a class="hvr-grow" href="<?php echo get_field('linkedin' , $datos_contacto); ?>" target="_blank">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-06.svg"></img>
                      </a>
                      <a class="hvr-grow" href="<?php echo get_field('facebook', $datos_contacto); ?>" target="_blank">
                        <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-15.svg"></img>
                      </a>
                  </div>

                  <div class="col-xs-12 col-md-12 text-center pixel-section">
                    <span class="pixel-size"> Diseño y Hosting por:
                      <a class="hvr-grow" href="http://www.pixelcr.com/" target="_blank" style="padding-left: 12px;">
                        <img class="icon-size-pixel"  src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-05.svg"></img>
                      </a>
                    </span>
                  </div>
                </div>
            </div>
    </div>
    </div>
    </div>

  </footer>


    <script>
             new WOW().init();
    </script>

  </body>
</html>


<script type="text/javascript">



jQuery(".single-item").slick({
  dots: false,
  infinite: true,
  speed: 300,
  rows: 1,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>
