

<?php

$datos_contacto = 112;

 ?>

</div>
  <?php wp_footer();?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/functions.js"></script>

    <?php

    if ( function_exists( 'ot_get_option' ) ) {
    $fixed = ot_get_option( 'header_fixed' );

    if ($fixed == 1 ) {
  ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/header-fixed.js"></script>
  <?php
    }
    }
    ?>
  <footer>
<div class="container-fluid">
    <div class="row is-flex">
        <div class="col-xs-12 col-md-6 background-consultas ">
          <div class="text-white">
               <h2 class="title-upper text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s">Consultas</h2>
             </div>
             <div class="col-xs-12 col-md-12">
                 <?php echo do_shortcode('[contact-form-7 id="61" title="Formulario de contacto"]');?>
             </div>
           </div>
        <div class="col-xs-12 col-md-6 footer-background-contact">
          <div class="row footer-contact-height" id="idContact">
                <div class="col-xs-12 col-md-12 text-center title-upper margin-info wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> <h2>Contáctenos</h2></div>
                  <div class="col-xs-12 col-md-12 margin-info padding-left-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                    <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-14.svg">
                      <span class="icon-text"> <?php echo get_field('telefono_1', $datos_contacto) . ' | ' . get_field('telefono_2', $datos_contacto)  ; ?> </span>
                    </img>
                  </div>
                  <div class="col-xs-12 col-md-12 margin-info padding-left-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                    <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-03.svg">
                      <span class="icon-text"> <?php echo get_field('fax', $datos_contacto); ?> </span>
                    </img>
                  </div>
                  <div class="col-xs-12 col-md-12 margin-info padding-left-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                    <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-13.svg">
                      <span class="icon-text"> <?php echo get_field('correo_electronico', $datos_contacto); ?> </span>
                    </img>
                  </div>
                  <div class="col-xs-12 col-md-12 margin-info padding-left-info wow slideInLeft" data-wow-offset="10" data-wow-duration="1.4s">
                    <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-04.svg">
                      <span class="icon-text"> <?php echo get_field('direccion', $datos_contacto); ?> </span>
                    </img>
                  </div>
          </div>
          <div class="row text-center social-media">
            <a class="hvr-grow" href="<?php echo get_field('linkedin' , $datos_contacto); ?>" target="_blank">
              <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-06.svg"></img>
            </a>
            <a class="hvr-grow" href="<?php echo get_field('facebook', $datos_contacto); ?>" target="_blank">
              <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-15.svg"></img>
            </a>
          </div>
          <div class="row text-center pixel-section">
            <span class="pixel-size"> Diseño y Hosting por:
              <a class="hvr-grow" href="http://www.pixelcr.com/" target="_blank">
                <img class="icon-size-pixel"  src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-05.svg"></img>
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
</div>

  </footer>

    <script>
             new WOW().init();
    </script>

    <!-- Script de servicios -->
    <script type="text/javascript">



    jQuery(".single-item").slick({
      dots: false,
      infinite: true,
      speed: 300,
      rows: 1,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>

    <!-- Script de clientes-actuales  -->
    <script type="text/javascript">

      jQuery(document).ready(function($){
        jQuery('.owl-dot').each(function(){
          jQuery(this).children('span').text(jQquery(this).index()+1);
          });
      });

    var prevArrow = jQuery('#prevId');
    var nextArrow = jQuery('#nextId');

    jQuery(".item-clientes").slick({
      dots: true,
      customPaging : function(slider, i) {
      var thumb = jQuery(slider.$slides[i]).data();
      return '<a>'+(i+1)+'</a>';
      },
       infinite: false,
       speed: 300,
       rows: 4,
       slidesToShow: 4,
       slidesToScroll: 4,
       nextArrow: nextArrow,
       prevArrow: prevArrow,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>

    <!-- Script links -->
    <script type="text/javascript">

    var prevArrow = jQuery('.prev');
    var nextArrow = jQuery('.next');

    jQuery(".item-links").slick({
       dots: false,
       infinite: false,
       speed: 300,
       rows: 3,
       slidesToShow: 4,
       slidesToScroll: 4,
       nextArrow: nextArrow,
       prevArrow: prevArrow,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>

    <!-- Script noticias-publicaciones -->
    <script type="text/javascript">

      jQuery(document).ready(function($){
        jQuery('.owl-dot').each(function(){
          jQuery(this).children('span').text(jQquery(this).index()+1);
          });
      });

    var prevArrow = jQuery('#prevId-not-pub');
    var nextArrow = jQuery('#nextId-not-pub');

    jQuery(".item-notic-public").slick({
      dots: true,
      customPaging : function(slider, i) {
      var thumb = jQuery(slider.$slides[i]).data();
      return '<a>'+(i+1)+'</a>';
      },
       infinite: false,
       speed: 300,
       rows: 2,
       slidesToShow: 4,
       slidesToScroll: 4,
       nextArrow: nextArrow,
       prevArrow: prevArrow,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>

    <!-- Script miembros -->
    <script type="text/javascript">

    var prevArrow = jQuery('.prev');
    var nextArrow = jQuery('.next');

    jQuery(".item-miembros").slick({
       dots: false,
       infinite: true,
       speed: 300,
       slidesToShow: 4,
       slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>

    <script type="text/javascript">

    jQuery( "#btn-enviar" ).click(function()
    {
        jQuery("#idContact").css({'padding-bottom': '516px','transition-delay':'600ms'});
    });

    </script>

  </body>
</html>
