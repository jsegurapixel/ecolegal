<?php /* Template Name: Noticias-Articulos */ ?>
<?php get_header()?>

<!-- Noticias -->
<?php if ($_GET['id'] == 1) : ?>

  <div class="row">
    <?php echo do_shortcode('[rev_slider alias="noticia"]');?>
  </div>

  <div class="container" id="clientesid">
    <div class="row page-padding">
      <h2 class="title-upper main-font-color text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> destacadas</h2><br>
    </div>
    <div class="row">

      <div class="col-xs-12 col-md-12">
          <div class='container-slick'>
            <!-- imagenes -->
            <div class="item-notic-public wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">
                <?php
                  $postId = 348;
                  $post = get_post($postId);
                  $url = get_permalink( $post->ID );
                 ?>

                <?php
                  $args = array(
                      'posts_per_page' => -1,
                        'post_type' => 'noticia',
                        'orderby' => 'publish_date',
                        'order' => 'ASC',
                      );
                  $the_query = new WP_Query( $args );?>
                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                        <a href=" <?php echo $url . '?id=1&post=' . get_the_ID(); ?>" class="hvr-float" title="Ver noticia">
                          <div style="padding:15px;">
                          <img class="img-size" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-noticias-1">
                          <h4 class="main-font-color title-upper"> <?php echo the_title(); ?> </h4>
                          <span class="subtitle-color"> <p> <?php echo get_field("subtitulo_1") . " " . "|" . " " . get_field("subtitulo_2") ?> </p> </span>
                          <p class="the-content">
                            <?php
                               $my_excerpt = get_the_excerpt();
                               if ( $my_excerpt != '')
                               {
                                   echo substr($my_excerpt,0,100) . '...' ;
                               }
                               else
                               {
                                 echo 'No hay información para mostrar'; // Outputs the processed value to the page
                               }
                            ?>
                         </p>
                         </div>
                      </a>

                <?php endwhile; ?>
                <?php else : ?>
                  <p><?php _e( 'No hay información para mostrar.' ); ?></p>
                <?php endif; ?>
            </div>
            <!-- arrows -->
            <div>
              <center>
              <ul class="ul-line-display pagination-icon">
                <li id="prevId-not-pub" class="li-line-arrows hvr-float"> < </li>
                <li id="nextId-not-pub" class="li-line-arrows hvr-float"> > </li>
              </ul>
              </center>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>

<!-- Publicaciones -->
<?php if ($_GET['id'] == 2): ?>
  <div class="row">
    <?php echo do_shortcode('[rev_slider alias="articulo"]');?>
  </div>

  <div class="container" id="clientesid">
    <div class="row page-padding">
      <h2 class="title-upper main-font-color text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> Destacados</h2><br>
    </div>
    <div class="row">

      <div class="col-xs-12 col-md-12">
          <div class='container-slick'>
            <!-- imagenes -->
            <div class="item-notic-public wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">
                <?php
                  $postId = 348;
                  $post = get_post($postId);
                  $url = get_permalink( $post->ID );
                 ?>

                <?php
                  $args = array(
                      'posts_per_page' => -1,
                        'post_type' => 'publicacion_articulo',
                        'orderby' => 'publish_date',
                        'order' => 'ASC',
                      );
                  $the_query = new WP_Query( $args );?>
                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                        <a href=" <?php echo $url . '?id=2&post=' . get_the_ID(); ?>" class="hvr-float" title="Ver noticia">
                          <div style="padding:15px;">
                          <img class="img-size" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-publicacion-articulos">
                          <h4 class="main-font-color title-upper"> <?php echo the_title(); ?> </h4>
                          <p class="the-content">
                            <?php
                               $my_excerpt = get_the_excerpt();
                               if ( $my_excerpt != '')
                               {
                                   echo substr($my_excerpt,0,100) . '...' ;
                               }
                               else
                               {
                                 echo 'No hay información para mostrar'; // Outputs the processed value to the page
                               }
                            ?>
                         </p>
                         </div>
                      </a>

                <?php endwhile; ?>
                <?php else : ?>
                  <p><?php _e( 'No hay información para mostrar.' ); ?></p>
                <?php endif; ?>
            </div>
            <!-- arrows -->
            <div>
              <center>
              <ul class="ul-line-display pagination-icon">
                <li id="prevId-not-pub" class="li-line-arrows hvr-float"> < </li>
                <li id="nextId-not-pub" class="li-line-arrows hvr-float"> > </li>
              </ul>
              </center>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>



<?php get_footer('2'); ?>
