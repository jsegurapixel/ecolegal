<?php /* Template Name: Nuestros-miembros */ ?>

<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="nuestros-miembros"]');?>
</div>
<!-- seccion 1 -->
<?php
  $id=167;
  $post = get_post($id);
  $seccion_nuestros_miembros = apply_filters('the_content', $post->post_content);
  $title = $post->post_title;
?>


<!-- link a miembro-detalle -->
<?php
  $id=396;
  $post = get_post($id);
  $url_detalle = get_permalink( $post->ID );
?>




<div class="row container-background text-center page-padding">
  <div class="container">
    <div class="wow fadeIn text-justify" data-wow-offset="10" data-wow-duration="1.7s"> <p> <?php echo $seccion_nuestros_miembros; ?> </div> </span>
  </div>
</div>
<!-- Nuestros miembros -->
<div class="row">

  <?php
    $id=167;
    $post = get_post($id);
    $field_1 = get_field_object('ordenar_por_nuestros_miembros');
    $field_2 = get_field_object('ordenar_nuestros_miembros');

  ?>


  <?php
  $args = array(
    'posts_per_page' => -1,
    'post_type' => 'equipo',
    'orderby' => $field_1['value'],
    'order' => $field_2['value'],
  );

  $the_query = new WP_Query( $args );
  $cont = 0; ?>
  <?php if ( $the_query->have_posts() ) : ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

  <?php if($cont > 3){ ?>
  <div class="col-xs-12 col-md-3  miembros-padding wow fadeInDown" data-wow-offset="10" data-wow-duration="2s">
    <?php if(has_post_thumbnail()) { ?>
        <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-equipo-trabajo-1">
    <?php }else { ?>
        <img class="img-responsive" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/10/fondo-blanco.jpg" alt="imagen-equipo-trabajo-1">
    <?php } ?>
    <br>
    <?php if (get_field('experiencia') && have_rows('areas_de_practica') && have_rows('educacion') && have_rows('idiomas')): ?>
        <a class="title-upper text-gray miembros-title hvr-grow" href=" <?php echo $url_detalle . '?id=' . get_the_ID(); ?> ">  <?php echo the_title(); ?> </a>
      <?php else : ?>
        <p class="title-upper text-gray miembros-title">  <?php echo the_title(); ?> </p>
    <?php endif; ?>

    <span class="title-upper text-gray"><?php the_content() ?></span>
    <hr>
      <p>
        <a class="hvr-grow" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
      </p>
      <p>
        <?php
          if (get_field('telefono') != null) {
        ?>
        <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
      <?php } ?>
      </p>
  </div><br>
<?php } else

{ ?>

  <div class="col-xs-12 col-md-3 background-gray miembros-padding wow fadeInUp" data-wow-offset="10" data-wow-duration="2s">
    <?php if(has_post_thumbnail()) { ?>
        <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-equipo-trabajo-1">
    <?php }else { ?>
        <img class="img-responsive" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/10/fondo-blanco.jpg" alt="imagen-equipo-trabajo-1">
    <?php } ?>
    <br>
    <?php if (get_field('experiencia') && have_rows('areas_de_practica') && have_rows('educacion') && have_rows('idiomas')): ?>
        <a class="title-upper text-gray miembros-title hvr-grow" href=" <?php echo $url_detalle . '?id=' . get_the_ID(); ?> ">  <?php echo the_title(); ?> </a>
      <?php else : ?>
        <p class="title-upper text-gray miembros-title">  <?php echo the_title(); ?> </p>
    <?php endif; ?>
    <span class="title-upper text-gray"><?php the_content() ?></span>
    <hr>
      <p>
        <a class="hvr-grow" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
      </p>
      <p>
        <?php
          if (get_field('telefono') != null) {
        ?>
        <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
      <?php } ?>
      </p>
  </div>


<?php  } ?>
  <?php


    $cont= $cont + 1;

   ?>
  <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

    <?php else : ?>
    <p><?php _e( 'No se ha encontrado información' ); ?></p>
    <?php endif; ?>

</div>
<!-- titulo abogados -->
<div class="row background-gray-dark text-white">
  <h2 class="text-center title-upper wow fadeIn page-padding" data-wow-offset="10" data-wow-duration="1.7s"> Nuestros abogados aliados en centroamérica </h2>
</div>

<!-- Contenido de abogados -->
<div class="container-fluid">
  <div class="row">
    <?php
      $id=167;
      $post = get_post($id);
    ?>
    <br>
    <div class="col-xs-12 col-md-12">

          <?php if(have_rows('abogados')):?>
          <?php while(have_rows('abogados')) : the_row();
              $nombre = get_sub_field('nombre',167);
              $puesto = get_sub_field('puesto',167);
              $pais = get_sub_field('pais',167);
              ?>
              <div class="col-xs-12 col-md-4">
                <span>
                  <p class="wow fadeInLeft" data-wow-offset="10" data-wow-duration="1.7s"><?php echo $nombre ?></p>
                </span>
              </div>
              <div class="col-xs-12 col-md-4">
                <span>
                  <p class="wow fadeInRight" data-wow-offset="10" data-wow-duration="1.7s"><?php echo $puesto ?></p>
                </span>
              </div>
              <div class="col-xs-12 col-md-4 wow fadeIn flag-display" data-wow-offset="10" data-wow-duration="1.7s">

                  <?php
                    $image = get_sub_field('bandera',167);
                    $url = $image['url'];
                  ?>
                  <img class="flag-size" src="<?php echo $url; ?>" alt="img-bandera-abogados">
                  <p class="padding-pais"><?php echo $pais ?></p>

              </div>
              <div class="col-xs-12 col-md-12"> <hr> </div>
              <hr>
              <?php
                endwhile;
              ?>
          <?php
          else :
          endif;
          ?>
    </div>
  </div>
</div>




<?php get_footer("2"); ?>
