<?php
if ( function_exists( 'ot_get_option' ) ) {
$logotipo = ot_get_option( 'logo' );
$header_color = ot_get_option( 'color_header' );
$fixed = ot_get_option( 'header_fixed' );
$transparente = ot_get_option( 'header_transparente' );
$nav_color = ot_get_option( 'color_de_texto' );
$fuente = ot_get_option( 'fuente' );
$selector_fuente = ot_get_option( 'selector_de_fuente' );

}

?>

<style>
  header{
    background:<?php echo $header_color; ?>;
  }

  header nav ul li a{

    color: <?php echo $nav_color; ?>;

  }

  #nav-icon1 span,#nav-icon2 span,#nav-icon3 span,#nav-icon4 span {

  background:  <?php echo $nav_color; ?> !important;

}


<?php if ($transparente == 1): ?>

header {
  position: absolute;
  z-index: 99;
  background: transparent;
  left: 0;
}


<?php endif; ?>


</style>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
      Ecolegal
    </title>
    <link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/hover.css" rel="stylesheet"  media="all">
      <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/hover-min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick-theme.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/mobile.css" rel="stylesheet">


    <?php
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

  function getBrowser($user_agent){

      if(strpos($user_agent, 'MSIE') !== FALSE)
          return 'Internet explorer';
      elseif(strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
          return 'Microsoft Edge';
      elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
          return 'Internet explorer';
      elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
          return "Opera Mini";
      elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
          return "Opera";
      elseif(strpos($user_agent, 'Firefox') !== FALSE)
          return 'Mozilla Firefox';
      elseif(strpos($user_agent, 'Chrome') !== FALSE)
          return 'Google Chrome';
      elseif(strpos($user_agent, 'Safari') !== FALSE)
          return "Safari";
      else
          return 'No hemos podido detectar su navegador';

  }

  $navegador = getBrowser($user_agent);
    ?>


    <?php if ($navegador == 'Safari'): ?>
      <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/safari.css" rel="stylesheet">
    <?php endif; ?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php


    if($fuente!=""){

    echo $fuente;
    ?>

    <style media="screen">

      body{
        font-family: <?php echo $selector_fuente;?>
      }
    </style>

    <?php

    }

    ?>
    <?php wp_head(); ?>
</head>

<body>
  <header id="header" >
    <ul class="top-bar">
      <li>
        <a class="rounded hvr-grow" href="tel:(506) 2256-0911" target="_blank">
          <i class="fa fa-phone" data-fa-transform="rotate-90"></i>

        </a>
      </li>
      <li>
        <a  class="rounded hvr-grow" href="https://cr.linkedin.com/company/ecolegal" target="_blank">
          <i class="fab fa-linkedin-in" ></i>
        </a>
      </li>
      <li>
        <a class="rounded hvr-grow" href="https://www.facebook.com/EcolegalConsultores/" target="_blank"><i class="fab fa-facebook-f"></i></a>
      </li>
    </ul>

<div class="container-fluid">

<div class="row">
    <div class="main-menu">

    <a href="<?php echo get_site_url(); ?>">
      <!-- <img class="wow rollIn" data-wow-duration="1s" data-wow-delay="2s" src="<?php echo $logotipo; ?>" id="logotipo" class="img-responsive" alt=""> -->
      <img src="<?php echo $logotipo; ?>" id="logotipo" class="img-responsive hvr-grow wow slideInLeft" data-wow-offset="10" data-wow-duration="2s" alt="">

    </a>
    <div class="hidden-md hidden-lg">
      <div class="hamburguer nav"  id="nav-icon2">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>


    <?php wp_nav_menu( array('menu' => 'Main', 'container' => 'nav' )); ?>

    <div class="search">

      <div class="col-xs-12 col-md-3">
        <h3 class="active">Aros Oftálmicos</h3>

      </div>

      <div class="col-xs-12 col-md-3">

        <h3>Lentes de sol</h3>
      </div>

      <div class="col-xs-12 col-md-3">
          <h3>Marca</h3>
      </div>

      <div class="col-xs-12 col-md-3">
        <h3>Polarizado</h3>
      </div>

      <div class="col-xs-12 col-md-3" id="aros">



      </div>

      <div class="col-xs-12 col-md-3">

      </div>

      <div class="col-xs-12 col-md-3">

      </div>
  </div>
    </div>
    </div>

</div>

  </header>


    <div class="container-fluid">
