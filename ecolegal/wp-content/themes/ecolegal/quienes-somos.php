<?php /* Template Name: Quienes-somos */ ?>

<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="quienes-somos"]');?>
</div>

<?php
  $id=122;
  $post = get_post($id);
  $title = $post->post_title;
  $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
?>
<!-- Historia -->
  <div class="row background-gray ">
    <div class="container">



    <div class="col-xs-12 col-md-12 txt-center">
      <h2 class="title-upper main-font-color wow zoomIn page-padding" data-wow-offset="10" data-wow-duration="1.4s">
        historia
      </h2>
      <span>
        <p class="wow fadeInRight text-justify" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo get_field('historia',122); ?></p><br>
      </span>
    </div>

    </div>
  </div>


<!-- Imagenes  -->
    <div class="row">
      <div class="col-xs-12 col-md-12 no-padding">
          <div class="col-xs-12 col-md-6 wow fadeInLeft no-padding" data-wow-offset="10" data-wow-duration="1.4s">
            <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-87.jpg" alt="img-quienes-somos-1" class="img-responsive img-quienessomos">
          </div>
          <div class="col-xs-12 col-md-6 wow fadeInRight no-padding" data-wow-offset="10" data-wow-duration="1.4s">
            <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-88.jpg" alt="img-quienes-somos-2" class="img-responsive img-quienessomos">
          </div>
      </div>
    </div>
<!-- Mision y vision -->
<div class="row txt-center">
      <div class="col-xs-12 col-md-6 container-background img-quienes-somos wow fadeInLeft" data-wow-offset="10" data-wow-duration="1.4s">
        <h2 class="title-upper wow zoomIn page-padding" data-wow-offset="10" data-wow-duration="1.4s"> Misión </h2>
        <span> <p class="wow fadeInRight" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo get_field("mision",122); ?> </p> </span>
      </div>
      <div class="col-xs-12 col-md-6 background-gray img-quienes-somos wow fadeInRight" data-wow-offset="10" data-wow-duration="1.4s">
        <h2 class="title-upper wow zoomIn text-gray page-padding" data-wow-offset="10" data-wow-duration="1.4s" > visión </h2>
        <span> <p class="wow fadeInRight" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo get_field("vision",122); ?>  </p> </span>
      </div><br>

</div>
<!-- Valores -->
  <div class="row break">
    <div class="col-xs-12 col-md-12 background-valores txt-center text-white page-padding">
      <div class="">


      <h2 class="title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.4s"> Valores </h2>
      <ul class="ul-line">
          <?php if(have_rows('valores')):?>
          <?php while(have_rows('valores')) : the_row();
              $valor = get_sub_field('valor'); ?>
              <li class="li-line wow fadeInDown" data-wow-offset="10" data-wow-duration="1.7s">
                <!-- <span> -->
                  <img class="icon-size" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-18-1.svg" alt="imagen-check-valores">
                  <p><?php echo $valor ?></p>
                <!-- </span> -->
              </li>
              <?php
                endwhile;
              ?>
          <?php
          else :
          endif;
          ?>
      </ul>
</div>

    </div>
  </div>


<div class="row break"></div>

<?php get_footer('2'); ?>
