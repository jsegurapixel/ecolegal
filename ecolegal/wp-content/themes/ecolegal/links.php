<?php /* Template Name: Links */ ?>


<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="link"]');?>
</div>


<div class="container" id="clientesid">
  <div class="row">
  </div>
  <div class="row">

    <div class="col-xs-12 col-md-12">
      <div class='container-slick'>
        <!-- imagenes -->
        <div class='item-links'>
            <?php

              $args = array(
                  'posts_per_page' => -1,
                    'post_type' => 'link',
                    'orderby' => 'title',
                    'order' => 'ASC',
                  );
              $the_query = new WP_Query( $args );?>
            <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                  <div class="item-content-logo animated fadeInDown">
                    <?php
                      $image = get_field('imagen');
                      $url = $image['url'];
                    ?>
                    <a class="hvr-grow" href="<?php echo get_field('url-cliente'); ?>"  target="_blank" title=" <?php echo the_title(); ?>">
                      <img class="brand-size" src="<?php echo $url; ?>" alt="imagen-links">
                    </a>
                  </div>
            <?php endwhile; ?>
            <?php else : ?>
              <p><?php _e( 'No hay información para mostrar.' ); ?></p>
            <?php endif; ?>
        </div>
        <ul class="link-arrows custome-arrows">
              <li class="prev hvr-grow"> <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-20.svg" alt=""> </li>
              <li class="next hvr-grow"> <img src="http://queleparece.com/ecolegal/wp-content/uploads/2018/08/rt-19.svg" alt=""></li>
        </ul>
      </div>
    </div>


    </div>
  </div>
<div class="row break"></div>

<?php get_footer("2"); ?>
