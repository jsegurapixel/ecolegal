<?php get_header()?>
<!-- Noticias -->
  <div class="row">
    <?php echo do_shortcode('[rev_slider alias="noticia"]');?>
  </div>

  <div class="container-fluid container-no-padding" style="padding-right: 0px;">
    <div class="row">
      <div class="col-xs-12 col-md-12 no-padding">
        <div class="col-xs-12 col-md-8">
          <?php
            $id= get_the_ID();
            $post = get_post($id);
            $content= apply_filters('the_content', $post->post_content);
            $title = $post->post_title;
            $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
          ?>
              <h2 class="main-font-color title-upper wow fadeIn page-padding" data-wow-offset="10" data-wow-duration="1.7s"><?php echo $title; ?></h2>
                <p> <?php echo get_field("subtitulo_1",$post) . " " . "|" . " " . get_field("subtitulo_2",$post); ?>  </p>
              <hr>
              <span class="text-gray text-justify wow fadeIn content-articulos-noticias" data-wow-offset="10" data-wow-duration="1.7s"> <p class="padding-articulos-noticias"> <?php echo $content; ?></p> </span>


        </div>
        <!-- Mas Noticias -->
        <div class="col-xs-12 col-md-4 background-noticia min-height-articulos-noticias page-padding">
          <div class="row">
            <div class="col-xs-12 col-md-12 text-center"><h2 class="main-font-color title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">Noticias</h2></div>
            <?php
              $args = array(
                'posts_per_page' => 4,
                'post_type' => 'noticia',
                'orderby' => 'publish_date',
                'order' => 'DESC',
              );
              $the_query = new WP_Query( $args );?>

              <?php
                $postId = 348;
                $post = get_post($postId);
                $url = get_permalink( $post->ID );
               ?>

            <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

              <div class="row wow fadeInRight row-no-margin" data-wow-offset="10" data-wow-duration="1.4s">
                <div class="col-xs-12 col-md-4 ">
                  <div class="date-form">
                    <center> <strong><h1 class="text-white">
                      <br>
                        <?php $date = 'j'; ?>
                        <?php echo get_the_date($date); ?>
                      </h1></strong>
                      <p class="text-white">
                        <?php $date = 'F'; ?>
                        <?php echo get_the_date($date); ?>
                      </p> </center>
                  </div>
                </div>
                <div class="col-xs-12 col-md-8">
                      <a  class="buttom-style hvr-grow" href="<?php echo $url .'/?id=1&post=' . get_the_ID(); ?>"> <h4 class="main-font-color title-upper"> <?php echo the_title(); ?> </h4> </a>
                      <span class="subtitle-color"> <p> <?php echo get_field("subtitulo_1") . " " . "|" . " " . get_field("subtitulo_2") ?> </p> </span>
                     <span class="the-content">
                       <p>
                         <?php
                            $my_excerpt = get_the_excerpt();
                            if ( $my_excerpt != '')
                            {
                              echo substr($my_excerpt,0,150) . '...' ;
                            }
                            else {
                              echo 'No hay información para mostrar'; // Outputs the processed value to the page
                            }
                          ?>
                       </p>
                   </span>
                </div>
              </div>
            <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>

              <?php else : ?>
              <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
              <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php get_footer('2')?>
