
<?php get_header()?>



  <div class="row">
    <?php echo do_shortcode('[rev_slider alias="articulo"]');?>
  </div>

  <div class="container-fluid container-no-padding" style="padding-right: 0px;">
    <div class="row">
      <div class="col-xs-12 col-md-12 no-padding">
        <div class="col-xs-12 col-md-9">
          <?php
            $id= get_the_ID();
            $post = get_post($id);
            $content= apply_filters('the_content', $post->post_content);
            $title = $post->post_title;
            $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
          ?>

            <h2 class="main-font-color title-upper page-padding wow fadeIn" data-wow-offset="10" data-wow-duration="1.7s"><?php echo $title; ?></h2>
            <div class="col-xs-12 col-md-12">
              <p class="text-gray display-content-block"> <?php echo get_field("autor",$post) . " " . "|" . " ";?>  </p>
              <p class="text-gray display-content-block"> <?php echo the_date('d-m-Y');   ?></p>
              <hr>
            </div>
            <span class="text-gray text-justify wow fadeIn content-articulos-noticias" data-wow-offset="10" data-wow-duration="1.7s"> <p class="padding-articulos-noticias  wow fadeIn" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo $content; ?></p> </span>




        </div>
        <!-- Mas publicaciones -->
        <div class="col-xs-12 col-md-3 background-noticia min-height-articulos-noticias page-padding">
          <div class="row">
            <div class="col-xs-12 col-md-12 text-center"><h2 class="main-font-color title-upper wow zoomIn page-padding" data-wow-offset="10" data-wow-duration="1.7s">Más artículos</h2></div>
            <?php
              $args = array(
                'posts_per_page' => 3,
                'post_type' => 'publicacion_articulo',
                'orderby' => 'publish_date',
                'order' => 'DESC',
              );
              $the_query = new WP_Query( $args );?>

              <?php
                $postId = 348;
                $post = get_post($postId);
                $url = get_permalink( $post->ID );
               ?>

            <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

            <div class="col-xs-12 col-md-12 text-center">
            <div>
              <center>
                <img class="img-responsive publicacion-img-height wow fadeIn" data-wow-offset="10" data-wow-duration="1.7s" src="<?php echo get_the_post_thumbnail_url(); ?>"  alt="imagen-publicacion">
                <br></center>
            </div>
              <div>
                <a href="<?php echo $url . '?id=2&post='. get_the_ID(); ?>" class="buttom-style main-font-color title-upper wow fadeInUp hvr-grow" data-wow-offset="10" data-wow-duration="1.7s"><?php echo the_title(); ?></a>
                <span>
                  <p class="text-gray text-justify wow fadeInUp" data-wow-offset="10" data-wow-duration="1.7s">
                    <?php
                     $my_excerpt = get_the_excerpt();
                     if ( $my_excerpt != '')
                     {
                       echo substr($my_excerpt,0,200  ) . '...' ;
                     }
                     else {
                       echo 'No hay información para mostrar'; // Outputs the processed value to the page
                     }
                     ?>

                  </p>
                </span>
                <br>
              </div>
          </div>
            <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>

              <?php else : ?>
              <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
              <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php get_footer('2')?>
