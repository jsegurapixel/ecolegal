
<?php get_header()?>



  <div class="row">
    <?php echo do_shortcode('[rev_slider alias="servicio"]');?>
  </div>


<?php
  $id= get_the_ID();
  $post = get_post($id);
  $content= apply_filters('the_content', $post->post_content);
  $title = $post->post_title;
  $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
?>


<?php if(have_rows('tipo_servicio',$id)):?>
  <?php while(have_rows('tipo_servicio',$id)) : the_row();
    $valor = get_sub_field('logo',$id);
    $url_logo = $valor['url'];
  ?>
  <div class="single-item-background background-gray content-servicios">
    <center><img class="img-size-servicio wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s" src="<?php echo $url_logo; ?>" alt="img-logo-servicios-ecolegal"></center>
    <h2 class="text-gray text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> <?php echo the_title(); ?></h2><br>
    <div class="text-justify content-font-servicios content-padding-servicios">
      <ul class="wow fadeInUp" data-wow-offset="10" data-wow-duration="1.7s">
        <?php while(have_rows('descripcion_servicio',$id)) : the_row();
        ?>
        <li>  <?php  the_sub_field('descripcion',$id); ?></li>
        <?php endwhile; ?>
      </ul>
    </div>
  </div>
  <?php
    endwhile;
  ?>

  <?php
  else :
  endif;
  ?>

<?php get_footer('2')?>
