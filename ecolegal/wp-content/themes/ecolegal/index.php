<?php get_header()?>

<div class="row">
  <?php echo do_shortcode('[rev_slider alias="home"]');?>
</div>

<!-- Servicios -->
<div class="row page-padding">
  <div class="row">
    <div class="container">
        <div>
          <div class="txt-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> <h2 class="title-upper main-font-color">Nuestros servicios</h2></div>
        </div>
    </div>
  </div>

  <div class="row">
    <div class="container row servicios-margin wow fadeInUp" data-wow-offset="10" data-wow-duration="1.5s">
      <?php
        $id=172;
        $post = get_post($id);
        $get_url = get_permalink($id);
      ?>

      <?php if(have_rows('lista_servicio')):?>
      <?php while(have_rows('lista_servicio')) : the_row();
          $img = get_sub_field('logo_servicio',172);
          $url_img = $img['url'];
          $nombre =  get_sub_field('nombre_del_servicio',172);  ?>
          <div class="txt-center col-xs-12 col-md-3 " >
            <br>
              <img id="img-animate" class="img-center img-responsive img-size-servicio" src="<?php echo $url_img; ?>" alt="imagen-servicio">
            <br>
            <p><?php echo $nombre; ?></p>
            <hr class="colorhr">
          </div>
          <?php
            endwhile;
          ?>
      <?php
      else :
      endif;
      ?>
        <center>
          <a class="buttom-option a-buttom-style hvr-grow" href="<?php echo $get_url; ?>">Ingresar > </a>
        </center>
        <br>
    </div>
  </div>

</div>
<!-- Servicios -->
<!-- Sobre nosotros -->
    <?php
      $id=122;
      $post = get_post($id);
      $sobrenosotros = apply_filters('the_content', $post->post_content);
      $title = $post->post_title;
      $img_nosotros_home_url = wp_get_attachment_url( get_post_thumbnail_id($id) );
    ?>

<div class="row container-background page-padding">
  <div class="container ">
      <div class="txt-center col-xs-12 col-md-12 wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">
        <h2 class="title-upper">
          <?php echo $title ?>
        </h2>
          <p class="page-padding-bottom"> <?php echo $sobrenosotros; ?></p>

  </div>
  </div>
</div>
<!-- Sobre nosotros -->
<!-- Nuestros miembros -->
<!-- link a miembro-detalle -->
<?php
  $id=396;
  $post = get_post($id);
  $url_detalle = get_permalink( $post->ID );
?>

<div class="page-padding">
  <div class="row">
    <div class="container">
        <div class="col-xs-12 col-md-12">
          <div class="txt-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s"> <h2 class="title-upper">Nuestro Equipo</h2></div>
          <br>
        </div>
    </div>
  </div>

  <div class="row">
    <div class="container">
      <?php
        $id=167;
        $post = get_post($id);
        $field_1 = get_field_object('ordenar_por_nuestros_miembros');
        $field_2 = get_field_object('ordenar_nuestros_miembros');

      ?>


      <?php
      $args = array(
        'posts_per_page' => -1,
        'post_type' => 'equipo',
        'orderby' => $field_1['value'],
        'order' => $field_2['value'],
      );

        $the_query = new WP_Query( $args );?>

        <div class='container-1-miembros'>
          <div class='single-item'>

      <?php if ( $the_query->have_posts() ) : ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

        <div class="col-xs-12 col-md-3 wow fadeInDown" data-wow-offset="10" data-wow-duration="2s">
          <?php if(has_post_thumbnail()) { ?>
              <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="imagen-equipo-trabajo">
          <?php }else { ?>
              <img class="img-responsive" src="http://queleparece.com/ecolegal/wp-content/uploads/2018/10/fondo-blanco.jpg" alt="imagen-equipo-trabajo-1">
          <?php } ?>
          <br>
          <?php if (get_field('experiencia') && have_rows('areas_de_practica') && have_rows('educacion') && have_rows('idiomas')): ?>
              <a class="title-upper text-gray miembros-title hvr-grow" href=" <?php echo $url_detalle . '?id=' . get_the_ID(); ?> ">  <?php echo the_title(); ?> </a>
            <?php else : ?>
              <p class="title-upper text-gray miembros-title">  <?php echo the_title(); ?> </p>
          <?php endif; ?>
          <span class="title-upper text-gray"><?php the_content() ?></span>
          <hr>
            <p>
              <a class="hvr-grow" href="mailto:<?php echo get_field('correo_electronico'); ?>"> <?php echo get_field('correo_electronico');?> </a>
            </p>
            <p>
              <?php
                if (get_field('telefono') != null) {
              ?>
              <a class="hvr-grow" href="tel:<?php echo get_field('telefono'); ?>"> <?php echo "Tel:" . get_field('telefono')?> </a>
            <?php } ?>
            </p>
        </div>

      <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>

        <?php else : ?>
        <p><?php _e( 'No hay información para mostrar' ); ?></p>
        <?php endif; ?>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Nuestros miembros -->
<!-- Consultas -->
<div class="row is-flex">

      <div class="col-xs-12 col-md-6 background-consultas page-padding consultas-min-height">
        <!-- echo . wp_get_attachment_url(65); . ?> -->
        <div class="text-white">
          <h2 class="title-upper text-center wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">Consultas</h2>
          <br>
        </div>
        <div class="col-xs-12 col-md-12 no-padding">
            <?php echo do_shortcode('[contact-form-7 id="61" title="Formulario de contacto"]');?>
        </div>
        <br>
      </div>

        <div class="col-xs-12 col-md-6 background-consultas-imagen-muestra consultas-min-height wow fadeInRight" data-wow-offset="10" data-wow-duration="1.7s" ></div>
</div>
<!-- Consultas -->
<!-- Publicaciones y articulos - noticias -->

  <div class="">
    <div class="row">
    <div class="col-xs-12 col-md-8 min-height-articulos-noticias">

      <div class="col-xs-12 col-md-12 page-padding">
        <div class="txt-center"> <h2 class="main-font-color title-upper wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">Publicaciones y artículos</h2></div>
        <br>
      </div>

        <?php
          $args = array(
            'posts_per_page' => 2,
            'post_type' => 'publicacion_articulo',
            'orderby' => 'publish_date',
            'order' => 'DESC',
          );
          $the_query = new WP_Query( $args );?>

          <?php
            $postId = 348;
            $post = get_post($postId);
            $url = get_permalink( $post->ID );
           ?>

        <?php if ( $the_query->have_posts() ) : ?>

        <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

          <div class="col-xs-12 col-md-6">
            <div>
                <img class="img-responsive publicacion-img-width wow fadeIn" data-wow-offset="10" data-wow-duration="1.7s" src="<?php echo get_the_post_thumbnail_url(); ?>"  alt="imagen-publicacion">
                <br>
            </div>
              <div>
                <a href="<?php echo $url . '?id=2&post='. get_the_ID(); ?>" class="main-font-color title-upper wow fadeInUp hvr-grow" data-wow-offset="10" data-wow-duration="1.7s"><?php echo the_title(); ?></a>
                <span>
                  <p class="text-gray text-justify wow fadeInUp" data-wow-offset="10" data-wow-duration="1.7s">
                    <?php
                     $my_excerpt = get_the_excerpt();
                     if ( $my_excerpt != '')
                     {
                       echo substr($my_excerpt,0,200  ) . '...' ;
                     }
                     else {
                       echo 'No hay información para mostrar'; // Outputs the processed value to the page
                     }
                     ?>

                  </p>
                </span>
                <br>
              </div>

          </div>

        <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>

          <?php else : ?>
          <p><?php _e( 'No hay información...' ); ?></p>
          <?php endif; ?>

          <br>
          <div class="text-center page-padding">
            <?php
              $postId = 344;
              $post = get_post($postId);
              $url = get_permalink( $post->ID );
             ?>

            <a href="<?php echo $url . '?id=2'; ?>" class="hvr-grow buttom-option a-buttom-style" >Ver Todos > </a>
          </div>
    </div>
    <!-- Publicaciones y articulos - noticias -->
    <!-- Noticias -->
    <div class="col-xs-12 col-md-4 background-noticia min-height-articulos-noticias">


      <div class="row">
        <div class="col-xs-12 col-md-12 text-center page-padding"><h2 class="main-font-color title-upper padding-bottom-noticias wow zoomIn" data-wow-offset="10" data-wow-duration="1.7s">Noticias</h2></div>

          <?php
            $args = array(
              'posts_per_page' => 3,
              'post_type' => 'noticia',
              'orderby' => 'publish_date',
              'order' => 'DESC',
            );
            $the_query = new WP_Query( $args );
          ?>

          <?php
            $postId = 348;
            $post = get_post($postId);
            $url = get_permalink( $post->ID );
           ?>

           <?php
             $noticia_id = 344;
             $post_noticia = get_post($noticia_id);
             $url_noticias = get_permalink( $post_noticia->ID ); ?>

        <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

          <div class="row wow fadeInRight row-no-margin" data-wow-offset="10" data-wow-duration="1.4s">
            <div class="col-xs-12 col-md-4 ">
              <div class="date-form">
                <center> <strong><h1 class="text-white">
                  <br>
                    <?php $date = 'j'; ?>
                    <?php echo get_the_date($date); ?>
                  </h1></strong>
                  <p class="text-white">
                    <?php $date = 'F'; ?>
                    <?php echo get_the_date($date); ?>
                  </p> </center>
              </div>
            </div>

            <div class="col-xs-12 col-md-8">
                  <a  class="buttom-style hvr-grow" href=" <?php echo $url . '?id=1&post=' . get_the_ID();  ?> "> <h4 class="main-font-color title-upper"> <?php echo the_title(); ?> </h4> </a>
                  <span class="subtitle-color"> <p> <?php echo get_field("subtitulo_1") . " " . "|" . " " . get_field("subtitulo_2"); ?> </p> </span>
                 <span class="the-content">
                   <p>
                     <?php
                        $my_excerpt = get_the_excerpt();
                        if ( $my_excerpt != '')
                        {
                          echo substr($my_excerpt,0,150) . '...' ;
                        }
                        else {
                          echo 'No hay información para mostrar'; // Outputs the processed value to the page
                        }
                      ?>
                   </p>
               </span>
            </div>
          </div>

        <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>

          <?php else : ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>
      </div>
      <center class="page-padding">
        <a href="<?php echo $url_noticias . '?id=1'; ?>" class="hvr-grow buttom-option a-buttom-style" >Ver Todos > </a>

      </center>
    </div>
  </div>
</div>
  <!-- Noticias -->

<?php get_footer()?>
